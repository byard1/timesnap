OUT=out

.PHONY: clean run

run:
	docker-compose up && docker-compose down

clean:
	docker-compose down
	for i in extractor loader transformer; \
		do docker image rm -f timesnap_$$i; done
	rm -f out/*.csv
