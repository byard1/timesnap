# Timesnap

Snap irregular time series data to a regular meter.

## Run in Docker

Execute the `run` make target to run this project in Docker.

```bash
make run
```

## Load In

Raw data CSV files found in the `csv` directory are [loaded into the
database](sql/load.sql).  A two column CSV file with timestamp and value
columns is expected ([example](csv/example.csv)).  You can drop your own data
files into this directory.

## Transform

The raw timestamps are [truncated into a regular time
series](sql/transform.sql).  Multiple raw values per interval are averaged.
The previous average is used for gaps.

## Extract Out

Filtered data as CSV files are [placed in the `out`
directory](sql/extract.sql).  You should find corresponding CSV files for any
you placed in `csv` here.

## Clean Up

The `clean` make target will clean up the project's data and docker images.

## Authors

* **Josh Byard** - *Initial work* - [jbyard](https://gitlab.com/jbyard)
