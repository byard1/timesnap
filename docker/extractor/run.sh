#!/bin/bash
#
# run.sh - I MUST extract the data!

SLEEP=1

export PGDATABASE=timesnap
export PGHOST=postgres
export PGPASSWORD=timesnap
export PGUSER=postgres

echo "I MUST transform the data!!"

while :; do

for CSV in /tmp/csv/*.csv; do
BASENAME=$(basename "$CSV" .csv)

{ psql -qATc <<SQL
	SELECT filtered.extract_table('$BASENAME')
SQL
} > /dev/null

done

sleep $SLEEP

done
