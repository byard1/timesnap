#!/bin/bash
#
# run.sh - I MUST load the data!

SLEEP=1
SOURCE=/tmp/csv/example.csv

export PGDATABASE=timesnap
export PGHOST=postgres
export PGPASSWORD=timesnap
export PGUSER=postgres

echo "I MUST load the data!!"

while :; do

for CSV in /tmp/csv/*.csv; do
BASENAME=$(basename "$CSV" .csv)

{ psql -qATc <<SQL
	SELECT raw.load_table('$BASENAME','$CSV')
SQL
} > /dev/null

done

sleep $SLEEP

done
