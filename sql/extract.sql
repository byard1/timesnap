CREATE SCHEMA IF NOT EXISTS filtered;

CREATE OR REPLACE FUNCTION filtered.extract_table( TEXT )
RETURNS VOID AS $$
BEGIN

	IF (
		SELECT COUNT(*) = 0 FROM information_schema.tables
		WHERE table_schema = 'filtered' AND table_name = $1
	) THEN RAISE EXCEPTION 'Filtered data for: % is not ready yet', $1;
	END IF;

	EXECUTE format('
		COPY filtered.%1$s TO ''/tmp/out/%1$s.csv'' WITH CSV HEADER
	',$1);

END;
$$ LANGUAGE PLPGSQL;

COMMENT ON FUNCTION filtered.extract_table IS
'Extract a filtered data table to a CSV file';
