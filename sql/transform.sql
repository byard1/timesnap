CREATE SCHEMA IF NOT EXISTS transform;
CREATE SCHEMA IF NOT EXISTS filtered;

CREATE OR REPLACE FUNCTION transform.table( TEXT )
RETURNS VOID AS $$
BEGIN

	IF (
		SELECT COUNT(*) = 0 FROM information_schema.tables
		WHERE table_schema = 'raw' AND table_name = $1
	) THEN RAISE EXCEPTION 'Raw data for: % is not ready yet', $1;
	END IF;

	EXECUTE format('
		CREATE TABLE IF NOT EXISTS filtered.%s (
			ts      TIMESTAMP,
			value   NUMERIC
		)
	',$1);

	EXECUTE format('
		DELETE FROM filtered.%s
	',$1);

	EXECUTE format($sql$
		INSERT INTO filtered.%1$s (ts, value) (
			WITH timeseries (t) AS (

							/* Regualar time series bounded by the raw data */
							SELECT generate_series(
											date_trunc('second',MIN(ts)),
											date_trunc('second',MAX(ts)),
											'1 second'::INTERVAL
							) FROM raw.%1$s

			)

			/* Join truncated raw data onto the regular time series */
			SELECT
							t,
							/* fudge any gaps smaller than 2 seconds */
							COALESCE(AVG(value), lag(AVG(value)) OVER (), lag(AVG(value),2) OVER ())
			FROM timeseries
			LEFT JOIN raw.example ON timeseries.t = date_trunc('second',ts)
			GROUP BY t
		)
	$sql$,$1);

END;
$$ LANGUAGE PLPGSQL;

COMMENT ON FUNCTION transform.table IS
'Transform raw data to a filtered data table';
