CREATE SCHEMA IF NOT EXISTS raw;

CREATE OR REPLACE FUNCTION raw.load_table( table_name TEXT, source_file TEXT )
RETURNS VOID AS $$
BEGIN
	EXECUTE format('
		CREATE TABLE IF NOT EXISTS raw.%s (
			ts      TIMESTAMP,
			value   NUMERIC
		)
	',$1);

	EXECUTE format('
		DELETE FROM raw.%s
	',$1);

	EXECUTE format('
		COPY raw.%s FROM ''%s'' WITH CSV HEADER
	',$1,$2);

END;
$$ LANGUAGE PLPGSQL;

COMMENT ON FUNCTION raw.load_table IS
'Load a table from a raw CSV file';
